Changes
=======

1.2
---

* Included tests in the pypi package.

1.1
---

* Fixed README enconding.
* Included documentation in the pypi package.

1.0
---

* Stable release.
* Documentation improvements.

0.8
---

* First release under new maintainer.
* Merged two siphashc module implementations.
